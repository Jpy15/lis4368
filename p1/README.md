# LIS4368 - Advanced Web Application Development

## John Yohe

### Project 1 Requirements:

Four Parts:

1. Clone Files from Bitbucket
2. Add Data Fields
3. Add Images to Carosel
4. Validate Data Fields
5. Chapter Questions (Chs 9,10)

#### README.md file should include the following items:

* Assignment Description
* Screenshot of Added Fields
* Screenshot of Validated Data

#### Assignment Screenshots:

*Screenshot of Added Fields:

* ![P1 Added Fields] (img/failedvalidation.png)

*Screenshot of Validated Data:

* ![P1 Validated Data] (img/passedvalidation.png)