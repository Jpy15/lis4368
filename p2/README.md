# LIS4368 - Advanced Web Application Development

## John Yohe

### Project 2 Requirements:

Five Parts:

1. Clone Files from Bitbucket
2. Work with MVC Pattern
3. Prepared statements to prevent SQL injection
4. Use JSTL to prevent XSS
5. Enable CRUD functionality


#### Assignment Screenshots:

*Screenshot of Valid User Form Entry:

* ![Valid Entry] (img/ss1.png)

*Screenshot of Passed Validation:

* ![p2 Passed Validation] (img/ss2.png)

*Screenshot of displayed data

* ![p2 displayed data] (img/ss3.png)

*modified form

* ![p2 modified] (img/ss4.png)

*modified form2

* ![p2 modified form] (img/ss5.png)

*Screenshot of delete prompt

* ![p2 deleted data] (img/ss6.png)

*Screenshot of database changes

* ![p2 displayed data] (img/ss7.png)






