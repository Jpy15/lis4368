> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## John Yohe

### Assignment 2 Requirements:

Three Parts:

1. MySQL downloaded and Installed
2. New User Created in MySQL
3. Develop and Deploy a WebApp.
4. Chapter Questions (Chs 5,6)

#### README.md file should include the following items:

* Screenshot of http://localhost:9999/hello (displays directory, index.html)
* Screenshot of http://localhost:9999/hello (displays HelloHome.html (changed to index.html))
* Screenshot of http://localhost:9999/hello/sayhello (Invokes HelloServlet, displays a random number.)
* Screenshot of http://localhost:9999/hello/querybook (displays list of authors with checkboxes)
* Screenshot of http://localhost:9999/hello/sayhi (displays /sayhello again with changed heading)
* Screenshot of http://localhost:9999/hello/querybook (with item selected)
* Screenshot of Query Results


#### Assignment Screenshots:

*Screenshots of http://localhost:9999/hello*:

* [Index](http://localhost:9999/hello/ "HelloHome/Index")

* [sayhello](http://localhost:9999/hello/sayhello "sayhello")

* [Query Book](http://localhost:9999/hello/querybook.html "QueryBook")

* [SayHi](http://localhost:9999/hello/sayhi "sayhi")

* ![Query Results] (img/queryresults.png)