# LIS4368 - Advanced Web Application Development

## John Yohe

### Assignment 4 Requirements:

Five Parts:

1. Clone Files from Bitbucket
2. Add Data Fields
3. Provide basic server-side validation
4. Provide screenshots for failed and passed validation
5. Chapter Questions (Chs 11, 12)

#### README.md file should include the following items:

* Assignment Description
* Screenshot of  Failed Validation
* Screenshot of Validated Data

#### Assignment Screenshots:

*Screenshot of Failed Validation:

* ![A4 Failed Validation] (img/failedvalidation.png)

*Screenshot of Validated Data:

* ![A4 Validated Data] (img/passedvalidation.png)