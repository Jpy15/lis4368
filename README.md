# LIS4368 Advanced Web Application Development

## John Yohe

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Git
    - Set up SSH for Git
    - Install Java JDK

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Download and Install MySQL
	- Create a new user in MySQL
	- Finish Tomcat Tutorial
	- Develop and Deploy a Webapp

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Download and Install MySQL Workbench
	- Create Databases for Pet Stores, Customers, and Pets
	- Forward Engineer a Database
	- Export an SQL Script

4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Create an Online Portfolio Displaying Skills Learned in A1-A3
	- Add Additional Fields to Input Data
	- Edit Carousel Images
	- Validate Data

5. [A4 README.md](a4/README.md "My A4 README.md file")
	- Download and Compile various files.
	- Add Additional Fields to Input Data
	- Provide Basic Server-Side Validation.
	- Provide Screenshots of Failed and Passed Validation

6. [A5 README.md](a5/README.md "My A5 README.md file")
	- Download and Compile CRUD Files
	- Connect to MYSQL Database.
	- Add fields to Customer Database
	- Provide Screenshots of pre and post user entry
7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Clone assignment starter files
	- Work with MVC Pattern
	- Enable full CRUD funtionality
	- Use JSTL to prevent XSS
