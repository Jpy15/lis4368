# LIS4368 - Advanced Web Application Development

## John Yohe

### Assignment 3 Requirements:

Three Parts:

1. Download and Install MySQL Workbench
2. Create ERD Containing Pet Store, Customer, and Pet information.
3. Forward Engineer Database
4. Create SQL Script
5. Chapter Questions (Chs 7,8)

#### README.md file should include the following items:

* Assignment Description
* Screenshot of A3 ERD
* Links to the following files: 
  a. a3.mwb
  b. a3.sql

Links

* [A3 MWB File](docs/a3.mwb "A3 MWB File")
* [A3 MySQL File](docs/a3.sql "A3 SQL File")


#### Assignment Screenshots:

*Screenshot of Database ERD*:

* ![A3 ERD] (img/a3erd.png)