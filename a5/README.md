# LIS4368 - Advanced Web Application Development

## John Yohe

### Assignment 5 Requirements:

Five Parts:

1. Clone Files from Bitbucket
2. Compile Various CRUD files
3. Connect to MYSQL Database
4. Provide screenshots for pre and post user form entry
5. Chapter Questions (Chs 13-15)

#### README.md file should include the following items:

* Assignment Description
* Screenshot of  Valid User Form Entry
* Screenshot of Passed Validation
* Screenshot of Associated Database Entry

#### Assignment Screenshots:

*Screenshot of Valid User Form Entry:

* ![A5 Valid User Form Entry] (img/formentry.png)

*Screenshot of Passed Validation:

* ![A5 Passed Validation] (img/passedvalidation.png)

*Screenshot of Associated Database Entry

* ![A5 Database Entry] (img/dbentry.png)


